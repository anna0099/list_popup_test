$(document).ready(function () {
    $('#list').click(function (event) {
        event.preventDefault();
        $('#products .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#products .item').removeClass('list-group-item');
        $('#products .item').addClass('grid-group-item');
    });
});

const btns = document.querySelectorAll('.btn-success')
const container = document.querySelector('.container')
const numOfPieces = 6 * 6;

const frag = document.createDocumentFragment();
const $popupsCont = document.querySelector('.popups-cont');
const h4 = document.querySelector('.popup__heading')
const $popup = document.querySelector('.popup');
const popupAT = 900;
const p = document.querySelector('.popup__text')
const allCards = document.querySelectorAll(' .list-group-item-text')

function insertInnerPieces($el, innerPieces) {
    for (let i = 0; i < innerPieces; i++) {
        let $inner = document.createElement('div');
        $inner.classList.add('popup__piece-inner');
        $el.appendChild($inner);
    }
}

for (let i = 1; i <= numOfPieces; i++) {
    let $piece = document.createElement('div');
    $piece.classList.add('popup__piece');

    insertInnerPieces($piece, 3);
    frag.appendChild($piece);
}

document.querySelector('.popup__pieces').appendChild(frag);

function closeHandler() {
    $popupsCont.classList.remove('s--popup-active');
    $popup.classList.remove('s--active');
    $popup.classList.add('s--closed');
    container.classList.remove('opacity')

    setTimeout(function () {
        $popup.classList.remove('s--closed');
    }, popupAT);
}

const users = [
    {
        "userId": 1,
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "userId": 1,
        "id": 2,
        "title": "qui est esse",
        "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae  porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
        "userId": 1,
        "id": 3,
        "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
    },
    {
        "userId": 1,
        "id": 4,
        "title": "eum et est occaecati",
        "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
    },
    {
        "userId": 1,
        "id": 5,
        "title": "nesciunt quas odio",
        "body": "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
    },
    {
        "userId": 1,
        "id": 6,
        "title": "dolorem eum magni eos aperiam quia",
        "body": "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
    }
]

document.querySelector('.popup__close').addEventListener('click', closeHandler);

document.querySelector('.popups-cont__overlay').addEventListener('click', closeHandler);
btns.forEach(function (btn) {
        btn.addEventListener('click', function () {
            $popupsCont.classList.add('s--popup-active');
            $popup.classList.add('s--active');
            container.classList.add('opacity')
            const currentId = btn.getAttribute('data-user-id')
            const currentUser = users.find(user => user.id.toString() === currentId)

            h4.innerText = currentUser.title
            h4.setAttribute('id', currentUser.id)
            p.innerText = currentUser.body
            p.setAttribute('id', currentUser.id)
        });
    }
)


allCards.forEach(function (card) {
    const currentId = card.getAttribute('data-text-id')
    const currentCard = users.find(user => user.id.toString() === currentId)
    card.innerText = currentCard.body
    card.setAttribute('id', currentCard.id)
})








